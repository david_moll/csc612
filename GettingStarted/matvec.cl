﻿// David Moll dmoll@emich.edu
// COSC 612 - Parallel Algorithms
// HW 0212
// Due 2/28/2018
// Adapted from NVidia OpenCL sample code (https://developer.nvidia.com/opencl)

__kernel void matvec_mult(__global float* matrix, __global float* vector,
                        __global float* result, uint width, uint height) {
    // get the row index
    uint y = get_global_id(0);

    if (y < height) {
        // get a pointer to the row
        const __global float* row = matrix + y * width;

        // Compute dot product  
        float dotProduct = 0;
        for (int x = 0; x < width; x++)
            dotProduct += row[x] * vector[x];

        // Write result to global memory
        result[y] = dotProduct;    
    }
}