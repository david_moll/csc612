﻿// David Moll dmoll@emich.edu
// HW 0212
// COSC 612 - Parallel Algorithms
// Due 2/28/2018
// Adapted from Listing 1-1 in "OpenCL in Action" by Matthew Scarpino
// Additional resources credited in-line.

#define PROGRAM_FILE "matvec.cl"
#define KERNEL_FUNC "matvec_mult"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <windows.h>

#ifdef MAC
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#define EIGHT           8 
#define SIXTEEN         16 
#define THIRTYTWO       32 
#define SIXTYFOUR       64 
#define ONETWENTYEIGHT  128
#define TWOFIFTYSIX     256
#define FIVETWELVE      512
#define TENTWENTYFOUR   1024
#define TWENTY48        2048
#define SQUARE64        4096
#define SQUARE128       16384
#define SQUARE256       65536
#define SQUARE512       262144
#define SQUARE1024      1048576
#define SQUARE2048      4194304
#define SQUARE4096      16777216

double parallelMatVec(float *mat, float *vec, float *result, size_t dimension);
double sequentialMatVecMult(float *mat, float *vec, float *result, size_t dimension);

int main() {

    // perform 4x4 matrix multiplied by 4x1 vector
    float smallVec[4];
    float smallRes[4];
    float smallMat[16];
    size_t smallDim = 4;

    sequentialMatVecMult(smallMat, smallVec, smallRes, smallDim);
    parallelMatVec(smallMat, smallVec, smallRes, smallDim);

    // perform 32x32 matrix multiplied by 34x1 vector
    float vector3[THIRTYTWO];
    float sequentialResult3[THIRTYTWO];
    float matrix3[TENTWENTYFOUR];
    size_t dim3 = THIRTYTWO;

    sequentialMatVecMult(matrix3, vector3, sequentialResult3, dim3);
    parallelMatVec(matrix3, vector3, sequentialResult3, dim3);

    // perform 64 x 64 matrix multiplied by 64x1 vector and measure how long it takes
    float vector2[SIXTYFOUR];
    float sequentialResult2[SIXTYFOUR];
    float matrix2[SQUARE64];
    size_t dim2 = SIXTYFOUR;

    sequentialMatVecMult(matrix2, vector2, sequentialResult2, dim2);
    parallelMatVec(matrix2, vector2, sequentialResult2, dim2);

    // perform 128 x 128 matrix multiplied by 128x1 vector and measure how long it takes
    float vector1[ONETWENTYEIGHT];
    float sequentialResult1[ONETWENTYEIGHT];
    float matrix1[SQUARE128];
    size_t dim1 = ONETWENTYEIGHT;

    sequentialMatVecMult(matrix1, vector1, sequentialResult1, dim1);
    parallelMatVec(matrix1, vector1, sequentialResult1, dim1);

    // perform 256 x 256 matrix multiplied by 256 x 1 vector and measure how long it takes
    float vector[TWOFIFTYSIX];
    float sequentialResult[TWOFIFTYSIX];
    float matrix[SQUARE256];
    size_t dim = TWOFIFTYSIX;

    sequentialMatVecMult(matrix, vector, sequentialResult, dim);
    parallelMatVec(matrix, vector, sequentialResult, dim);
    
    // 512 x 512 matrix needs to be allocated directly
    float *vectorLarge = malloc(FIVETWELVE * sizeof(float));
    float *seqResultLarge = malloc(FIVETWELVE * sizeof(float));
    float *matrixLarge = malloc(SQUARE512 * sizeof(float));
    size_t dimLarge = FIVETWELVE;

    sequentialMatVecMult(matrixLarge, vectorLarge, seqResultLarge, dimLarge);
    parallelMatVec(matrixLarge, vectorLarge, seqResultLarge, dimLarge);

    free(vectorLarge);
    free(seqResultLarge);
    free(matrixLarge);

    // 1024 x 1024 matrix needs to be allocated directly
    float *vectorHuge = malloc(TENTWENTYFOUR * sizeof(float));
    float *seqResultHuge = malloc(TENTWENTYFOUR * sizeof(float));
    float *matrixHuge = malloc(SQUARE1024 * sizeof(float));
    size_t dimHuge = TENTWENTYFOUR;

    sequentialMatVecMult(matrixHuge, vectorHuge, seqResultHuge, dimHuge);
    parallelMatVec(matrixHuge, vectorHuge, seqResultHuge, dimHuge);

    free(vectorHuge);
    free(seqResultHuge);
    free(matrixHuge);

    // 2048 x 2048 matrix needs to be allocated directly
    float *vectorGig = malloc(TWENTY48 * sizeof(float));
    float *seqResultGig = malloc(TWENTY48 * sizeof(float));
    float *matrixGig = malloc(SQUARE2048 * sizeof(float));
    size_t dimGig = TWENTY48;

    sequentialMatVecMult(matrixGig, vectorGig, seqResultGig, dimGig);
    parallelMatVec(matrixGig, vectorGig, seqResultGig, dimGig);

    free(vectorGig);
    free(seqResultGig);
    free(matrixGig);

    // 4096 x 4096 matrix needs to be allocated directly
    float *vectorBiggest = malloc(SQUARE64 * sizeof(float));
    float *seqResultBiggest = malloc(SQUARE64 * sizeof(float));
    float *matrixBiggest = malloc(SQUARE4096 * sizeof(float));
    size_t dimBiggest = SQUARE64;

    sequentialMatVecMult(matrixBiggest, vectorBiggest, seqResultBiggest, dimBiggest);
    parallelMatVec(matrixBiggest, vectorBiggest, seqResultBiggest, dimBiggest);

    free(vectorBiggest);
    free(seqResultBiggest);
    free(matrixBiggest);

}

// Perform matrix-vector multiplication
// mat * vec = result
// dimension = # rows of matrix
// return time it took to perform the calculation
double sequentialMatVecMult(float *mat, float *vec, float *result, size_t dimension)
{
    size_t matrix_size = dimension * dimension;

    // initialize the mat and the vector
    for (int l = 0; l < dimension; l++)
    {
        vec[l] = l * 3;
        result[l] = 0;
    }
    for (int k = 0; k < matrix_size; k++)
    {
        mat[k] = k * 2;
    }

    // it turns out that measuring performance time on windows in C is *really* difficult
    // https://stackoverflow.com/questions/4568221/c-get-system-time-to-microsecond-accuracy-on-windows

    unsigned __int64 freq;
    QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
    double timerFrequency = (1.0 / freq);

    unsigned __int64 startTime;
    QueryPerformanceCounter((LARGE_INTEGER *)&startTime);

    // do the mat multiplication
    for (size_t j = 0; j < dimension; j++) {
        for (size_t m = 0; m < dimension; m++) {

            // Since we're storing a mat in a 1D array
            // we need to calculate the row offset
            size_t rowOffset = dimension * j;

            result[j] += mat[m + rowOffset] * vec[m];
        }
    }

    unsigned __int64 endTime;
    QueryPerformanceCounter((LARGE_INTEGER *)&endTime);
    double timeDifference = ((endTime - startTime) * timerFrequency);

    printf("SEQUENTIAL: %llu x %llu matrix multiplied by a vector took %f seconds.\n", dimension, dimension, timeDifference);

    return timeDifference;
}

// Setup an OpenCL kernel and use it to perform Matrix x Vector multiplication
// Computes matrix * vector
// Compares result to parameter correct to verify OpenCL got same result as sequential
// dimension = # rows in matrix
// returns time (in seconds) GPU took to do calculation
double parallelMatVec(float *matrix, float *vector, float *correct, size_t dimension) {
    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_int err;

    cl_program program;
    FILE *program_handle;
    char *program_buffer, *program_log;
    size_t program_size, log_size;
    cl_kernel kernel;
    
    float *result = malloc(dimension * sizeof(float));
    cl_mem mat_buff, vec_buff, res_buff;
    size_t work_units_per_kernel;

    // initialize the result matrix
    for (int i = 0; i < dimension; i++)
    {
        result[i] = 0.0f;
    }

    // set platform / device / context
    //printf("Getting the platform id. \n");
    err = clGetPlatformIDs(1, &platform, NULL);
    if (err < 0) {
        perror("Couldn't find any platforms");
        exit(1);
    }
    //printf("Getting the devices.\n");
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
    if (err < 0) {
        perror("Couldn't find any devices");
        exit(1);
    }
    //printf("Getting the devices.\n");
    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
    if (err < 0) {
        perror("Couldn't create a context");
        exit(1);
    }
    // read program file
    //printf("Reading the program file.\n");

    // in windows we need to open the file in read-binary mode
    program_handle = fopen(PROGRAM_FILE, "rb");
    if (program_handle == NULL) {
        perror("Couldn't find the program file");
        exit(1);
    }
    fseek(program_handle, 0, SEEK_END);
    program_size = ftell(program_handle);
    rewind(program_handle);
    program_buffer = (char*)malloc(program_size + 1);
    program_buffer[program_size] = '\0';
    fread(program_buffer, sizeof(char), program_size, program_handle);
    fclose(program_handle);

    // compile program
    //printf("Compiling the program file.\n");
    program = clCreateProgramWithSource(context, 1,
        (const char**)&program_buffer, &program_size, &err);
    if (err < 0) {
        perror("Couldn't create the program");
        exit(1);
    }
    free(program_buffer);
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err < 0) {
        printf("Printing the error log.\n");
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        program_log = (char*)malloc(log_size + 1);
        program_log[log_size] = '\0';
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size + 1, program_log, NULL);
        printf("%s\n", program_log);
        free(program_log);
        exit(1);
    }
    // create kernel / queue
    //printf("Creating the kernel.\n");
    kernel = clCreateKernel(program, KERNEL_FUNC, &err);
    if (err < 0) {
        perror("Couldn't create the kernel");
        exit(1);
    }

    size_t matrixSize = dimension * dimension;

    mat_buff = clCreateBuffer(context, CL_MEM_READ_ONLY |
        CL_MEM_COPY_HOST_PTR, sizeof(float) * matrixSize, matrix, &err);
    if (err < 0) {
        perror("Couldn't create a buffer object");
        exit(1);
    }
    vec_buff = clCreateBuffer(context, CL_MEM_READ_ONLY |
        CL_MEM_COPY_HOST_PTR, sizeof(float) * dimension, vector, NULL);
    res_buff = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
        sizeof(float) * dimension, NULL, NULL);

    // set kernel arguments
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &mat_buff);
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &vec_buff);
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &res_buff);
    err |= clSetKernelArg(kernel, 3, sizeof(cl_int), &dimension);
    err |= clSetKernelArg(kernel, 4, sizeof(cl_int), &dimension);

    if (err < 0) {
        perror("Couldn't set the kernel argument");
        exit(1);
    }

    queue = clCreateCommandQueue(context, device, 0, &err);
    if (err < 0) {
        perror("Couldn't create the command queue");
        exit(1);
    }

    // set up for recording the time
    unsigned __int64 freq;
    QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
    double timerFrequency = (1.0 / freq);

    // we need one work unit for each row of the matrix
    work_units_per_kernel = dimension;

    // record the start time
    unsigned __int64 startTime;
    QueryPerformanceCounter((LARGE_INTEGER *)&startTime);
    
    err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &work_units_per_kernel,
        NULL, 0, NULL, NULL);

    // record the stop time
    unsigned __int64 endTime;
    QueryPerformanceCounter((LARGE_INTEGER *)&endTime);
    double timeDifference = ((endTime - startTime) * timerFrequency);

    printf("PARALLEL: %llu x %llu matrix multiplied by a vector took %f seconds.\n", dimension, dimension, timeDifference);

    if (err < 0) {
        perror("Couldn't enqueue the kernel execution command");
        exit(1);
    }
    
    // read the result of the OpenCL kernel 
    err = clEnqueueReadBuffer(queue, res_buff, CL_TRUE, 0, sizeof(float) * dimension,
        result, 0, NULL, NULL);
    if (err < 0) {
        perror("Couldn't enqueue the read buffer command");
        exit(1);
    }

    boolean success = TRUE;
    for (int i = 0; i < dimension; i++) {
        // if any of the elements don't match, success gets set to false
        success && (result[i] == correct[i]);
    }

    if (success) {
        printf("Matrix-vector multiplication succeeded!\n");
    }
    else {
        printf("Matrix-vector multiplication unsuccessful.\n");

        for (size_t i = 0; i < dimension; ++i) {
            printf("element %zu result is %f, \tcorrect is %f\n",
                i,
                result[i],
                correct[i]);
        }
    }

    clReleaseMemObject(mat_buff);
    clReleaseMemObject(vec_buff);
    clReleaseMemObject(res_buff);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseProgram(program);
    clReleaseContext(context);

    free(result);

    return timeDifference;
}