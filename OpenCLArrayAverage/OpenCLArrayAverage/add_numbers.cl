__kernel void add_numbers(__global float8* data, 
      __local float* local_result, __global float* group_result) {

   float sum;
   float8 input1, input2, input3, input4, sum_vector;
   float8 input5, input6, input7, input8;
   uint global_addr, local_addr;

   global_addr = get_global_id(0) * 8;
   input1 = data[global_addr];
   input2 = data[global_addr+1];
   input3 = data[global_addr + 2];
   input4 = data[global_addr + 3];
   input5 = data[global_addr + 4];
   input6 = data[global_addr + 5];
   input7 = data[global_addr + 6];
   input8 = data[global_addr + 7];
   sum_vector = input1 + input2 + input3 + input4 +
				input5 + input6 + input7 + input8;

   local_addr = get_local_id(0);
   local_result[local_addr] = sum_vector.s0 + sum_vector.s1 + 
                              sum_vector.s2 + sum_vector.s3 +
							  sum_vector.s4 + sum_vector.s5 +
							  sum_vector.s6 + sum_vector.s7;
   barrier(CLK_LOCAL_MEM_FENCE);

   if(get_local_id(0) == 0) {
      sum = 0.0f;
      for(int i=0; i<get_local_size(0); i++) {
         sum += local_result[i];
      }
      group_result[get_group_id(0)] = sum;
   }
}
